package czajkowski.patryk.distancecalculator.repository

import android.accounts.NetworkErrorException
import czajkowski.patryk.distancecalculator.BaseTest
import czajkowski.patryk.distancecalculator.model.AddressResponse
import czajkowski.patryk.distancecalculator.model.Location
import czajkowski.patryk.distancecalculator.model.LocationDao
import czajkowski.patryk.distancecalculator.model.LocationResponse
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import czajkowski.patryk.distancecalculator.api.LocationService
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException

@RunWith(MockitoJUnitRunner::class)
class LocationRepositoryTest  : BaseTest() {
    lateinit var repository: LocationRepository

    private val dao: LocationDao = mock()
    private val service: LocationService = mock()

    val locationResponse = LocationResponse(
        lat = 50.3423,
        lon = 2.912,
        address = AddressResponse(
            country = "France",
            village = "Guines",
            state = "Hauts-de-France",
            postcode = "62340.0"
        )
    )

    private val location = Location(
        id = 0L,
        lat = 50.3423,
        lon = 2.912,
        country = "France",
        village = "Guines",
        state = "Hauts-de-France",
        postcode = "62340.0"
    )

    @Before
    fun init() {
        repository = LocationRepository(service, dao)
    }

    @Test
    fun `should get location when api and dao contains some locations`() {
        whenever(service.getLocation(any(), any(), any())).thenReturn(Single.just(locationResponse))
        whenever(dao.getLocation(any(), any())).thenReturn(Observable.just(location))

        repository.getLocation(2.912, 50.3423)
            .test()
            .assertNoErrors()
            .assertValueAt(0, location)
            .assertValueAt(1, location)

        verify(service).getLocation(longitude = 2.912, latitude = 50.3423)
        verify(dao).getLocation(longitude = 2.912, latitude = 50.3423)
    }

    @Test
    fun `should not get location and return error when api throw error`() {
        whenever(dao.getLocation(any(), any())).thenReturn(Observable.just(location))
        whenever(service.getLocation(any(), any(), any())).thenReturn(Single.error(NetworkErrorException("500")))

        repository.getLocation(longitude = 2.912, latitude = 50.3423)
            .test()
            .assertError(NetworkErrorException::class.java)
    }

    @Test
    fun `should not get location and return error places when dao throw error`() {
        whenever(service.getLocation(any(), any(), any())).thenReturn(Single.just(locationResponse))
        whenever(dao.getLocation(any(), any())).thenReturn(Observable.error(IOException()))

        repository.getLocation(longitude = 2.912, latitude = 50.3423)
            .test()
            .assertError(IOException::class.java)
    }

    @Test
    fun `should update location when dao works fine`() {
        whenever(dao.updateLocation(any())).thenReturn(Completable.complete())

        repository.updateLocation(location)
            .test()
            .assertComplete()

        verify(dao).updateLocation(any())
    }

}
