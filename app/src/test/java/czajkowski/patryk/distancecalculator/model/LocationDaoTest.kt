package czajkowski.patryk.distancecalculator.model

import czajkowski.patryk.distancecalculator.BaseDaoTest
import io.objectbox.Box
import org.junit.Test

class LocationDaoTest : BaseDaoTest() {

    lateinit var dao: LocationDao
    lateinit var db: Box<Location>

    val location = Location(
        id = 0L,
        lat = 50.3423,
        lon = 2.912,
        country = "France",
        village = "Guines",
        state = "n/a",
        postcode = "343-36"
    )

    val locationUpdated = Location(
        id = 0L,
        lat = 50.3423,
        lon = 2.912,
        country = "France",
        village = "Guines",
        state = "Guines State",
        postcode = "343-3622"
    )

    override fun before() {
        db = store!!.boxFor(Location::class.java)
        dao = LocationDao(db)
    }

    @Test
    fun `should not return when location is not present in db`() {
        dao.getLocation(2.912, 50.3423)
            .test()
            .awaitCount(1)
            .assertValueCount(0)
    }

    @Test
    fun `should return location when is saved in db`() {
        db.put(location)

        dao.getLocation(2.912, 50.3423)
            .test()
            .awaitCount(1)
            .assertValueAt(0) { it.id == 1L }
            .assertValueCount(1)

    }

    @Test
    fun `should update location when a location with the same id is in db`() {
        db.put(location)

        dao.updateLocation(locationUpdated)
            .test()
            .await()
            .assertComplete()

        val location = dao.getLocation(2.912, 50.3423).blockingFirst()

        assert(location.state == locationUpdated.state)
        assert(location.postcode == locationUpdated.postcode)
    }

}