package czajkowski.patryk.distancecalculator.ui.typepoints

import czajkowski.patryk.distancecalculator.BaseTest
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TypePointsPresenterTest : BaseTest() {

    @Mock
    private lateinit var view: TypePointsContract.View

    lateinit var presenter: TypePointsPresenter

    @Before
    fun init() {
        presenter = TypePointsPresenter()
        presenter.attach(view)
    }

    @Test
    fun `should enable button when all inputs filled properly`() {
        presenter.setDestinationLatitude("57.0")
        presenter.setDestinationLongitude("34.0")
        presenter.setOriginLatitude("89.0")
        presenter.setOriginLongitude("179.0")

        verify(view).enableDisableButton(true)
    }

    @Test
    fun `should disable button when any input unfilled`() {
        presenter.setDestinationLongitude("34.0")
        presenter.setOriginLatitude("89.0")
        presenter.setOriginLongitude("179.0")

        verify(view, times(3)).enableDisableButton(false)
    }

    @Test
    fun `should return false when typed latitude is less than minus 90`() {
        assert(!presenter.isValidLatitude("-100"))
    }

    @Test
    fun `should return false when typed latitude is greater than 90`() {
        assert(!presenter.isValidLatitude("100"))
    }

    @Test
    fun `should return false when typed longitude is less than minus 180`() {
        assert(!presenter.isValidLongitude("-200"))
    }

    @Test
    fun `should return false when typed longitude is greater than 180`() {
        assert(!presenter.isValidLongitude("200"))
    }
}