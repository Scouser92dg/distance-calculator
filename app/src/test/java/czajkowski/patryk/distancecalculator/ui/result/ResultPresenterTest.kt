package czajkowski.patryk.distancecalculator.ui.result

import czajkowski.patryk.distancecalculator.BaseTest
import czajkowski.patryk.distancecalculator.model.Location
import czajkowski.patryk.distancecalculator.repository.LocationRepository
import czajkowski.patryk.distancecalculator.ui.model.LocationModelWrapper
import czajkowski.patryk.distancecalculator.util.LocationType
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ResultPresenterTest: BaseTest() {

    @Mock
    private lateinit var view: ResultContract.View

    lateinit var presenter: ResultPresenter

    private val locationRepository: LocationRepository = mock()
    private val error = Exception("Something wrong")

    private val location = Location(
        id = 0L,
        lat = 50.3423,
        lon = 2.912,
        country = "France",
        village = "Guines",
        state = "Hauts-de-France",
        postcode = "62340.0"
    )

    @Before
    fun init() {
        whenever(locationRepository.getLocation(longitude = 2.912, latitude = 50.3423)).thenReturn(Observable.just(location))
        whenever(locationRepository.updateLocation(any())).thenReturn(Completable.complete())

        presenter = ResultPresenter(locationRepository)
        presenter.attach(view)
    }

    @Test
    fun `origin should set after fetch location`() {
        presenter.fetchLocation(LocationType.ORIGIN, longitude = 2.912, latitude = 50.3423)
        Mockito.verify(view).setOriginLocation(LocationModelWrapper(location))
    }

    @Test
    fun `destination should set after fetch location`() {
        presenter.fetchLocation(LocationType.DESTINATION, longitude = 2.912, latitude = 50.3423)
        Mockito.verify(view).setDestinationLocation(LocationModelWrapper(location))
    }

    @Test
    fun `should not set origin location and show error when repository return error`() {
        whenever(locationRepository.getLocation(any(), any())).thenReturn(Observable.error(error))

        presenter.fetchLocation(LocationType.ORIGIN, longitude = 2.912, latitude = 50.3423)

        Mockito.verify(view, never()).setOriginLocation(LocationModelWrapper(location))
        Mockito.verify(view).showErrorMessage(error.message!!)

    }

    @Test
    fun `should not set destination and show error location when repository return error`() {
        whenever(locationRepository.getLocation(any(), any())).thenReturn(Observable.error(error))

        presenter.fetchLocation(LocationType.DESTINATION, longitude = 2.912, latitude = 50.3423)

        Mockito.verify(view, never()).setDestinationLocation(LocationModelWrapper(location))
        Mockito.verify(view).showErrorMessage(error.message!!)
    }

}