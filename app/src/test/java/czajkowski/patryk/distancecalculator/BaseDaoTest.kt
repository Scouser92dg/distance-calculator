package czajkowski.patryk.distancecalculator

import czajkowski.patryk.distancecalculator.model.MyObjectBox
import io.objectbox.BoxStore
import io.objectbox.DebugFlags
import org.junit.After
import org.junit.Before
import java.io.File

abstract class BaseDaoTest : BaseTest() {

    protected val TEST_DIRECTORY = File("objectbox-example/test-db")
    protected var store: BoxStore? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        BoxStore.deleteAllFiles(TEST_DIRECTORY)
        store = MyObjectBox.builder()
            .directory(TEST_DIRECTORY)
            .debugFlags(DebugFlags.LOG_QUERIES or DebugFlags.LOG_QUERY_PARAMETERS)
            .build()

        before()

    }

    abstract fun before()

    @After
    @Throws(Exception::class)
    fun tearDown() {
        if (store != null) {
            store!!.close()
            store = null
        }
        BoxStore.deleteAllFiles(TEST_DIRECTORY)
    }
}