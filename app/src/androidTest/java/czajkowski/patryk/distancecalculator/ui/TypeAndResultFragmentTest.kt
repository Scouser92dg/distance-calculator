package czajkowski.patryk.distancecalculator.ui

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import czajkowski.patryk.distancecalculator.R
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class TypeAndResultFragmentTest: BaseFragmentTest() {

    @Before
    fun init() {
        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.fragmentTypePointsLayout))
    }

    @Test
    fun checkIfButtonEnabledAfterAllFieldFilled() {
        fillAllFields()

        Espresso.onView(ViewMatchers.withId(R.id.calculateButton))
            .check(ViewAssertions.matches(ViewMatchers.isEnabled()))
    }

    private fun fillAllFields() {
        Espresso.onView(ViewMatchers.withId(R.id.originLongitudeEditText))
            .perform(ViewActions.typeText("20"))

        Espresso.onView(ViewMatchers.withId(R.id.originLatitudeEditText))
            .perform(ViewActions.typeText("50"))

        Espresso.onView(ViewMatchers.withId(R.id.destinationLongitudeEditText))
            .perform(ViewActions.typeText("112"))

        Espresso.onView(ViewMatchers.withId(R.id.destinationLatitudeEditText))
            .perform(ViewActions.typeText("20"), closeSoftKeyboard())
    }

    @Test
    fun checkIfButtonHiddenWhenOriginLongMiss() {
        Espresso.onView(ViewMatchers.withId(R.id.originLatitudeEditText))
            .perform(ViewActions.typeText("50"))

        Espresso.onView(ViewMatchers.withId(R.id.destinationLongitudeEditText))
            .perform(ViewActions.typeText("112"))

        Espresso.onView(ViewMatchers.withId(R.id.destinationLatitudeEditText))
            .perform(ViewActions.typeText("20"))

        Espresso.onView(ViewMatchers.withId(R.id.calculateButton))
            .check(ViewAssertions.matches(not(ViewMatchers.isEnabled())))
    }

    @Test
    fun checkIfButtonHiddenWhenOriginLatMiss() {
        Espresso.onView(ViewMatchers.withId(R.id.originLongitudeEditText))
            .perform(ViewActions.typeText("20"))

        Espresso.onView(ViewMatchers.withId(R.id.destinationLongitudeEditText))
            .perform(ViewActions.typeText("112"))

        Espresso.onView(ViewMatchers.withId(R.id.destinationLatitudeEditText))
            .perform(ViewActions.typeText("20"))

        Espresso.onView(ViewMatchers.withId(R.id.calculateButton))
            .check(ViewAssertions.matches(not(ViewMatchers.isEnabled())))
    }

    @Test
    fun checkIfButtonHiddenWhenDestLongMiss() {
        Espresso.onView(ViewMatchers.withId(R.id.originLongitudeEditText))
            .perform(ViewActions.typeText("20"))

        Espresso.onView(ViewMatchers.withId(R.id.originLatitudeEditText))
            .perform(ViewActions.typeText("50"))

        Espresso.onView(ViewMatchers.withId(R.id.destinationLatitudeEditText))
            .perform(ViewActions.typeText("20"))

        Espresso.onView(ViewMatchers.withId(R.id.calculateButton))
            .check(ViewAssertions.matches(not(ViewMatchers.isEnabled())))
    }

    @Test
    fun checkIfButtonHiddenWhenDestLatMiss() {
        Espresso.onView(ViewMatchers.withId(R.id.originLongitudeEditText))
            .perform(ViewActions.typeText("20"))

        Espresso.onView(ViewMatchers.withId(R.id.originLatitudeEditText))
            .perform(ViewActions.typeText("50"))

        Espresso.onView(ViewMatchers.withId(R.id.destinationLongitudeEditText))
            .perform(ViewActions.typeText("112"))

        Espresso.onView(ViewMatchers.withId(R.id.calculateButton))
            .check(ViewAssertions.matches(not(ViewMatchers.isEnabled())))
    }

    @Test
    fun checkIfGoToResultOnCalculateClick() {
        fillAllFields()

        Espresso.onView(ViewMatchers.withId(R.id.calculateButton))
            .perform(ViewActions.click())

        waitUntilViewIsDisplayed(ViewMatchers.withId(R.id.fragmentResultLayout))
    }


}

