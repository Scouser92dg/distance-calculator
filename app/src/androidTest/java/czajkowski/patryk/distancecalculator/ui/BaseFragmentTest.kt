package czajkowski.patryk.distancecalculator.ui

import android.view.View
import androidx.test.espresso.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import czajkowski.patryk.visitedplaces.ui.testutil.ViewIdlingResource
import org.hamcrest.Matcher
import org.junit.Rule

open class BaseFragmentTest {

    protected val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    protected fun waitUntilViewIsDisplayed(matcher: Matcher<View?>) {
        val idlingResource: IdlingResource =
            ViewIdlingResource(
                matcher,
                ViewMatchers.isDisplayed()
            )
        try {
            IdlingRegistry.getInstance().register(idlingResource)
            // First call to onView is to trigger the idler.
            Espresso.onView(ViewMatchers.withId(0)).check(ViewAssertions.doesNotExist())
        } finally {
            IdlingRegistry.getInstance().unregister(idlingResource)
        }
    }

}