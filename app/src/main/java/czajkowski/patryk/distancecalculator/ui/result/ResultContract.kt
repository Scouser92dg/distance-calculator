package czajkowski.patryk.distancecalculator.ui.result

import czajkowski.patryk.distancecalculator.BaseContract
import czajkowski.patryk.distancecalculator.ui.model.LocationModelWrapper
import czajkowski.patryk.distancecalculator.util.LocationType

class ResultContract {

    interface View {
        fun setOriginLocation(location: LocationModelWrapper)
        fun setDestinationLocation(location: LocationModelWrapper)
        fun showErrorMessage(message: String)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetchLocation(type: LocationType, longitude: Double, latitude: Double)
    }

}