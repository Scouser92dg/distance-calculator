package czajkowski.patryk.distancecalculator.ui.model

import czajkowski.patryk.distancecalculator.model.Location

data class LocationModelWrapper (
    var lat: Double? = null,
    var lon: Double? = null,
    var village: String = "",
    var state: String = "",
    var postcode: String = "",
    var country: String = ""
) {
    constructor(location: Location): this(location.lat, location.lon, location.village, location.state, location.postcode, location.country)
}