package czajkowski.patryk.distancecalculator.ui.typepoints

import android.content.Context
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import czajkowski.patryk.distancecalculator.R
import czajkowski.patryk.distancecalculator.ui.result.ResultFragment
import com.google.android.material.textfield.TextInputLayout
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_type_points.*
import org.koin.android.ext.android.inject


class TypePointsFragment : Fragment(), TypePointsContract.View {

    private val presenter: TypePointsContract.Presenter by inject()
    lateinit var disposables: CompositeDisposable

    override fun onAttach(context: Context) {
        super.onAttach(context)
        disposables = CompositeDisposable()
        presenter.attach(this@TypePointsFragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_type_points, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        calculateButton.setOnClickListener {
            presenter.onCalculateClicked()
        }

        observeInputs()
    }

    private fun observeInputs() {
        originLatitudeEditText.textChangeEvents()
            .subscribeBy {
                val value = it.text.toString()
                if (validateLatitude(originLatitudeLayout, value)) {
                    presenter.setOriginLatitude(value)
                } else {
                    presenter.setOriginLatitude("")
                }
            }
            .addTo(disposables)

        originLongitudeEditText.textChangeEvents()
            .subscribeBy {
                val value = it.text.toString()
                if (validateLongitude(originLongitudeLayout, value)) {
                    presenter.setOriginLongitude(value)
                } else {
                    presenter.setOriginLongitude("")
                }
            }
            .addTo(disposables)

        destinationLatitudeEditText.textChangeEvents()
            .subscribeBy {
                val value = it.text.toString()
                if (validateLatitude(destinationLatitudeLayout, value)) {
                    presenter.setDestinationLatitude(value)
                } else {
                    presenter.setDestinationLatitude("")
                }
            }
            .addTo(disposables)

        destinationLongitudeEditText.textChangeEvents()
            .subscribeBy {
                val value = it.text.toString()
                if (validateLongitude(destinationLongitudeLayout, value)) {
                    presenter.setDestinationLongitude(value)
                } else {
                    presenter.setDestinationLongitude("")
                }
            }
            .addTo(disposables)
    }

    private fun validateLongitude(view: TextInputLayout, text: String): Boolean {
        if (presenter.isValidLongitude(text)) {
            view.isErrorEnabled = false
            return true
        } else {
            if (view.editText?.isFocused == true) {
                view.isErrorEnabled = true
                view.error = getString(R.string.error_longitude_invalid)
            }
            return false
        }
    }

    private fun validateLatitude(view: TextInputLayout, text: String): Boolean {
        if (presenter.isValidLatitude(text)) {
            view.isErrorEnabled = false
            return true
        } else {
            if (view.editText?.isFocused == true) {
                view.isErrorEnabled = true
                view.error = getString(R.string.error_latitude_invalid)
            }
            return false
        }
    }

    override fun goToResult(
        originLocation: Location,
        destinationLocation: Location
    ) {
        val args = Bundle()
        args.putParcelable(ResultFragment.BUNDLE_ORIGIN, originLocation)
        args.putParcelable(ResultFragment.BUNDLE_DESTINATION, destinationLocation)
        activity?.findNavController(R.id.nav_host_fragment)
            ?.navigate(R.id.action_typePointsFragment_to_resultFragment, args)
    }

    override fun enableDisableButton(isEnable: Boolean) {
        calculateButton.isEnabled = isEnable
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
        disposables.clear()
    }


}