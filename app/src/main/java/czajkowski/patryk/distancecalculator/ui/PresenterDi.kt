package czajkowski.patryk.distancecalculator.ui

import czajkowski.patryk.distancecalculator.ui.result.ResultContract
import czajkowski.patryk.distancecalculator.ui.result.ResultPresenter
import czajkowski.patryk.distancecalculator.ui.typepoints.TypePointsContract
import czajkowski.patryk.distancecalculator.ui.typepoints.TypePointsPresenter
import org.koin.dsl.module

val presenterModule = module {

    single<TypePointsContract.Presenter> { TypePointsPresenter() }
    single<ResultContract.Presenter> { ResultPresenter(get()) }

}