package czajkowski.patryk.distancecalculator.ui.result

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import czajkowski.patryk.distancecalculator.R
import czajkowski.patryk.distancecalculator.ui.model.LocationModelWrapper
import czajkowski.patryk.distancecalculator.util.LocationType
import czajkowski.patryk.distancecalculator.util.setTextIfNotBlank
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_result.*
import org.koin.android.ext.android.inject

class ResultFragment : Fragment(), ResultContract.View {

    private val presenter: ResultContract.Presenter by inject()

    companion object {
        const val BUNDLE_ORIGIN = "bundle_origin"
        const val BUNDLE_DESTINATION = "bundle_destination"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.apply {
            presenter.attach(this@ResultFragment)
        }
        val originLocation = arguments?.getParcelable<android.location.Location>(BUNDLE_ORIGIN)
        val destinationLocation =
            arguments?.getParcelable<android.location.Location>(BUNDLE_DESTINATION)

        if (originLocation != null && destinationLocation != null) {
            presenter.fetchLocation(LocationType.ORIGIN, originLocation.longitude, originLocation.latitude)
            presenter.fetchLocation(LocationType.DESTINATION, destinationLocation.longitude, destinationLocation.latitude)

            val distanceMeters = originLocation.distanceTo(destinationLocation)
            resultMetersText.setText(distanceMeters.toString())
            resultKilometersText.setText((distanceMeters / 1000).toString())
        }
    }

    override fun setDestinationLocation(location: LocationModelWrapper) {
        resultDestinationCountryText.setTextIfNotBlank(location.country)
        resultDestinationVillageText.setTextIfNotBlank(location.village)
        resultDestinationStateText.setTextIfNotBlank(location.state)
        resultDestinationPostcodeText.setTextIfNotBlank(location.postcode)
    }

    override fun setOriginLocation(location: LocationModelWrapper) {
        resultOriginCountryText.setTextIfNotBlank(location.country)
        resultOriginVillageText.setTextIfNotBlank(location.village)
        resultOriginStateText.setTextIfNotBlank(location.state)
        resultOriginPostcodeText.setTextIfNotBlank(location.postcode)
    }

    override fun showErrorMessage(message: String) {
        Snackbar.make(fragmentResultLayout, message, Snackbar.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

}