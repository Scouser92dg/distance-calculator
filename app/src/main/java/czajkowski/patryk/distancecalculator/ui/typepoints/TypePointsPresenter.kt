package czajkowski.patryk.distancecalculator.ui.typepoints

import android.location.Location
import io.reactivex.disposables.CompositeDisposable


class TypePointsPresenter : TypePointsContract.Presenter {

    companion object {
        const val LOCATION_ORIGIN_LABEL = "origin"
        const val LOCATION_DESTINATION_LABEL = "destination"
    }

    private var view: TypePointsContract.View? = null
    var disposables = CompositeDisposable()

    private var destLatitude = ""
    private var destLongitude = ""
    private var origLatitude = ""
    private var origLongitude = ""

    override fun detach() {
        disposables.clear()
        this.view = null
    }

    override fun attach(view: TypePointsContract.View) {
        this.view = view
    }

    override fun setOriginLatitude(value: String) {
        origLatitude = value
        view?.enableDisableButton(areAllInputsFilled())
    }

    override fun setOriginLongitude(value: String) {
        origLongitude = value
        view?.enableDisableButton(areAllInputsFilled())
    }

    override fun setDestinationLatitude(value: String) {
        destLatitude = value
        view?.enableDisableButton(areAllInputsFilled())
    }

    override fun setDestinationLongitude(value: String) {
        destLongitude = value
        view?.enableDisableButton(areAllInputsFilled())
    }

    override fun onCalculateClicked() {
        val originLocation = Location(LOCATION_ORIGIN_LABEL)
        originLocation.latitude = origLatitude.toDouble()
        originLocation.longitude = origLongitude.toDouble()

        val destinationLocation = Location(LOCATION_DESTINATION_LABEL)
        destinationLocation.latitude = destLatitude.toDouble()
        destinationLocation.longitude = destLongitude.toDouble()
        view?.goToResult(originLocation, destinationLocation)
    }

    override fun isValidLatitude(value: String): Boolean {
        if (value.isBlank() || value == "." || value == "-" || value == "-.") return false
        return value.toDouble() >= -90.0 && value.toDouble() <= 90.0
    }

    override fun isValidLongitude(value: String): Boolean {
        if (value.isBlank() || value == "." || value == "-" || value == "-.") return false
        return value.toDouble() >= -180.0 && value.toDouble() <= 180.0
    }

    private fun areAllInputsFilled(): Boolean {
            return destLatitude.isNotBlank()
            && origLatitude.isNotBlank()
                && destLongitude.isNotBlank()
                && origLongitude.isNotBlank()
    }

}