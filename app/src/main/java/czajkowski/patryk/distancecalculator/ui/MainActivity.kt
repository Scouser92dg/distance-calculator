package czajkowski.patryk.distancecalculator.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import czajkowski.patryk.distancecalculator.R


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setTitle(R.string.app_name)
    }

}
