package czajkowski.patryk.distancecalculator.ui.typepoints

import android.location.Location
import czajkowski.patryk.distancecalculator.BaseContract


class TypePointsContract {

    interface View {
        fun enableDisableButton(isEnable: Boolean)
        fun goToResult(originLocation: Location, destinationLocation: Location)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun setOriginLatitude(value: String)
        fun setOriginLongitude(value: String)
        fun setDestinationLatitude(value: String)
        fun setDestinationLongitude(value: String)
        fun onCalculateClicked()
        fun isValidLatitude(value: String): Boolean
        fun isValidLongitude(value: String): Boolean
    }

}