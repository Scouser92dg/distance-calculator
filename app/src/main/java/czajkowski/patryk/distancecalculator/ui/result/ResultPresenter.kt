package czajkowski.patryk.distancecalculator.ui.result

import czajkowski.patryk.distancecalculator.repository.LocationRepository
import czajkowski.patryk.distancecalculator.ui.model.LocationModelWrapper
import czajkowski.patryk.distancecalculator.util.LocationType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class ResultPresenter(
    private val locationRepository: LocationRepository
) : ResultContract.Presenter {

    private lateinit var view: ResultContract.View
    lateinit var disposables: CompositeDisposable

    override fun fetchLocation(locationType: LocationType, longitude: Double, latitude: Double) {
        locationRepository.getLocation(longitude, latitude)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .debounce(500, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onError = {
                    it.message?.let {
                        view.showErrorMessage(it)
                    }
                },
                onNext = {
                    locationRepository.updateLocation(it)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                            onError = {
                                it.message?.let {
                                    view.showErrorMessage(it)
                                }
                            },
                            onComplete = {
                                val location = LocationModelWrapper(it)
                                if (locationType == LocationType.ORIGIN) {
                                    view.setOriginLocation(location)
                                } else if (locationType == LocationType.DESTINATION) {
                                    view.setDestinationLocation(location)
                                }
                            }
                        ).addTo(disposables)

                }
            ).addTo(disposables)
    }

    override fun detach() {
        disposables.dispose()
    }

    override fun attach(view: ResultContract.View) {
        this.view = view
        disposables = CompositeDisposable()
    }
}