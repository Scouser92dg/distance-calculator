package czajkowski.patryk.distancecalculator.util

enum class LocationType {
    ORIGIN, DESTINATION
}