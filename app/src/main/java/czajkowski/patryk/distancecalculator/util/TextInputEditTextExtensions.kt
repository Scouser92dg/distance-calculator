package czajkowski.patryk.distancecalculator.util

import com.google.android.material.textfield.TextInputEditText

fun TextInputEditText.setTextIfNotBlank(text: String) {
    if (text.isNotBlank() && text != "null") {
        this.setText(text)
    } else {
        this.setText("n/a")
    }
}
