package czajkowski.patryk.distancecalculator

import android.app.Application
import czajkowski.patryk.distancecalculator.api.serviceModule
import czajkowski.patryk.distancecalculator.model.daoModule
import czajkowski.patryk.distancecalculator.repository.repositoryModule
import czajkowski.patryk.distancecalculator.ui.presenterModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class DistanceCalculatorApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@DistanceCalculatorApp)
            modules(
                listOf(
                    serviceModule,
                    daoModule,
                    repositoryModule,
                    presenterModule
                )
            )
        }
    }

}