package czajkowski.patryk.distancecalculator.repository

import org.koin.dsl.module

val repositoryModule = module {
    single { LocationRepository(get(), get()) }
}