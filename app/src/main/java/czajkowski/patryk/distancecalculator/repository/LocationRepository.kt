package czajkowski.patryk.distancecalculator.repository

import android.util.Log
import czajkowski.patryk.distancecalculator.api.LocationService
import czajkowski.patryk.distancecalculator.model.Location
import czajkowski.patryk.distancecalculator.model.LocationDao
import czajkowski.patryk.distancecalculator.model.LocationResponse
import io.reactivex.*
import io.reactivex.schedulers.Schedulers

class LocationRepository (private val locationService: LocationService, private val locationDao: LocationDao) {

    fun getLocation(longitude: Double, latitude: Double): Observable<Location> {
        return Observable.concatArray(
            getLocationFromDb(longitude, latitude),
            getLocationFromApi(longitude, latitude)
        )
    }

    private fun getLocationFromDb(longitude: Double, latitude: Double): Observable<Location> {
        return locationDao.getLocation(longitude, latitude)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .doOnNext {
                Log.d("LocationRepository", "Dispatching location from DB ${it.id}")
            }
    }

    private fun getLocationFromApi(longitude: Double, latitude: Double): Observable<Location> {
        return locationService.getLocation(longitude = longitude, latitude = latitude)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .onErrorResumeNext { Single.error(it) }
            .toObservable()
            .map { mapResponseToLocation(it) }
            .doOnNext {
                Log.d("LocationRepository", "Dispatching location from API ${it.id}")
            }
    }

    private fun mapResponseToLocation(
        locationResponse: LocationResponse
    ): Location {
        return Location(
            lat = locationResponse.lat,
            lon = locationResponse.lon,
            village = locationResponse.address?.village ?: "",
            state = locationResponse.address?.state ?: "",
            postcode = locationResponse.address?.postcode ?: "",
            country = locationResponse.address?.country ?: ""
        )
    }

    fun updateLocation(location: Location): Completable {
        return locationDao.updateLocation(location)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
    }

}