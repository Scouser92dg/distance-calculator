package czajkowski.patryk.distancecalculator.api

import czajkowski.patryk.distancecalculator.model.LocationResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface LocationService {

    @GET("reverse")
    fun getLocation(
        @Query("format") format: String = "jsonv2",
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double
    ): Single<LocationResponse>

}