package czajkowski.patryk.distancecalculator.api

import czajkowski.patryk.distancecalculator.api.factory.GsonFactory
import czajkowski.patryk.distancecalculator.api.factory.OkHttpClientFactory
import czajkowski.patryk.distancecalculator.api.factory.RetrofitFactory
import org.koin.dsl.module
import retrofit2.Retrofit


val serviceModule = module {

    single {
        GsonFactory.createGson()
    }

    single {
        OkHttpClientFactory.createOkHttpClient()
    }

    single {
        RetrofitFactory.createRetrofit(get(), get())
    }

    single<LocationService> { get<Retrofit>().create(LocationService::class.java) }

}