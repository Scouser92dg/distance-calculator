package czajkowski.patryk.distancecalculator.model

import io.objectbox.BoxStore
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val daoModule = module {

    single { MyObjectBox.builder().androidContext(androidApplication()).build() }
    single { LocationDao(get<BoxStore>().boxFor(Location::class.java)) }

}