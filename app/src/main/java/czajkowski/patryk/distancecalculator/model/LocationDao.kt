package czajkowski.patryk.distancecalculator.model

import io.objectbox.Box
import io.reactivex.*

class LocationDao(private val database: Box<Location>) {

    companion object {
        const val POINT_TOLERANCE_VALUE = 1.0
    }

    fun getLocation(longitude: Double, latitude: Double): Observable<Location> {
        return Observable.fromCallable {
            database.query().equal(Location_.lon, longitude, POINT_TOLERANCE_VALUE)
                .and().equal(Location_.lat, latitude, POINT_TOLERANCE_VALUE)
                .build()
        }
            .flatMapMaybe { Maybe.fromCallable<Location> { it.findFirst() } }
            .distinctUntilChanged()
    }

    fun updateLocation(location: Location): Completable {
        return Single.fromCallable {
            database.all.filter {
                it.lon == location.lon && it.lat == location.lat
            }.firstOrNull()?.let {
                database.remove(it.id)
            }
            database.put(location)
            return@fromCallable location
        }.ignoreElement()
    }

}