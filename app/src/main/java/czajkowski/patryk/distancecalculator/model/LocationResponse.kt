package czajkowski.patryk.distancecalculator.model

data class LocationResponse (
    var lat: Double,
    var lon: Double,
    var address: AddressResponse?
)

data class AddressResponse (
    var village: String?,
    var state: String?,
    var postcode: String?,
    var country: String?
)