package czajkowski.patryk.distancecalculator.model

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class Location (
    @Id var id: Long = 0L,
    var lat: Double? = null,
    var lon: Double? = null,
    var village: String = "",
    var state: String = "",
    var postcode: String = "",
    var country: String = ""
)