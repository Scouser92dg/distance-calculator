package czajkowski.patryk.distancecalculator

class BaseContract {
    interface Presenter<in T> {
        fun detach()
        fun attach(view: T)
    }
}